/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paquete;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.HashSet;
import java.util.*;


public class Main {
    public static void main(String[] args) {
        //Crear las cuentas
        CuentaCorriente c1 = new CuentaCorriente("Rey Daiana", 8000, 1);
        CuentaCorriente c2 = new CuentaCorriente("Maria Ezpinoza", 7000, 2);
        CuentaCorriente c3 = new CuentaCorriente("Eduardo Gaona", 3000, 3);
        CuentaCorriente c4 = new CuentaCorriente("Nazarena Agustinelli", 6000, 4);
        CuentaCorriente c5 = new CuentaCorriente("Marco Agustinelli", 10000, 5);
        CuentaCorriente c6 = new CuentaCorriente("Adriana Alcara", 50, 6);
        CuentaCorriente c7 = new CuentaCorriente("Gabriela ROdriguez", 1000, 7);
        
        //Creamos la Colección HashSet
        Set <CuentaCorriente> clientesBanco = new HashSet<CuentaCorriente>();
        
        //Añadimos las cuentas a la colección
        clientesBanco.add(c1);
        clientesBanco.add(c2);
        clientesBanco.add(c3);
        clientesBanco.add(c4);
        clientesBanco.add(c5);
        clientesBanco.add(c6);
        clientesBanco.add(c7);
        
        //Guardamos los objetos en un archivo
        FileOutputStream archivo = null;
        try {
            archivo = new FileOutputStream("Cuentas.dat");
            ObjectOutputStream canal = new ObjectOutputStream(archivo);
            canal.writeObject(c1);
            canal.writeObject(c2);
            canal.writeObject(c3);
            canal.writeObject(c4);
            canal.writeObject(c5);
            canal.writeObject(c6);
            canal.writeObject(c7);
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch(IOException ex){
            ex.printStackTrace();
        }finally{
            try {
                archivo.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        
        
        //Mostramos la información de las cuentas
        for(CuentaCorriente cuentas : clientesBanco){
            System.out.println(cuentas.toString());
            System.out.println("\n");
        }
    }
}
