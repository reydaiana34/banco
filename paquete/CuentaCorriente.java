/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paquete;

import java.io.Serializable;

public class CuentaCorriente implements Serializable{
    private String titular;
    private double saldo;
    private int nrocuenta;
    
    public CuentaCorriente(String titular, double saldo, int nrocuenta){
        this.titular = titular;
        this.saldo = saldo;
        this.nrocuenta = nrocuenta;
    }
    public CuentaCorriente(){
        
    }
    
    public String getTitular() {
        return titular;
    }
    
    public double getSaldo() {
        return saldo;
    }
    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }
    public void retiro(double monto, CuentaCorriente cuenta){
        cuenta.setSaldo(saldo - monto);    
    }
   
    @Override
    public String toString() {
        return "Cuenta Corriente " + "titular= " + titular + ", saldo= " + saldo + ", nrocuenta= " + nrocuenta;
    }
    public boolean Transferencia(CuentaCorriente emisor, CuentaCorriente receptor, double monto){
        if (emisor.getSaldo() < monto){
            return false;
        }else{
            emisor.retiro(monto, receptor);
            receptor.setSaldo(saldo + monto);
            return true;
        }
    }
}
